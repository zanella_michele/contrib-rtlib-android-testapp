package it.polimi.deib.bosp.rtlibandroidtestapp;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "AndroidTestAppActivity";

    /** Flag indicating whether we have called bind on the service. */
    boolean mBound;

    /** Messenger for communicating with the service. */
    Messenger mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.w(TAG, "Starting TestAppService...");
        bindService(new Intent(this, TestAppService.class), mConnection,
                Context.BIND_AUTO_CREATE);
        Log.d(TAG, "onStart finished");
    }

    /** Class for interacting with the main interface of the service. */
    private final ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            mService = new Messenger(service);
            mBound = true;
            Log.i(TAG, "Service bound!");

            Log.d(TAG, "Starting EXC...");
            if (!mBound) return;
            //Prepare a message to set the number of onRun cycles to be performed
            Message cycles = Message.obtain(null, TestAppService.MSG_CYCLES, 0, 0);
            //Get the number of iterations from the "cycle_inputs" EditText.
            //String input = (((EditText)findViewById(
            //        R.id.cycles_input)).getText().toString());
            //If empty, assume 0.
            cycles.arg1 = 4;
            try {
                cycles.replyTo = mMessenger;
                mService.send(cycles);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            //Prepare a message to call the "EXCStart" native function.
            Message msg = Message.obtain(null, TestAppService.MSG_START, 0, 0);
            try {
                msg.replyTo = mMessenger;
                mService.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            mService = null;
            mBound = false;
        }
    };

    /**
     * Instantiate the target - to be sent to clients - to communicate with
     * this instance of Service
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());

    /**
     * Handler of incoming messages from Service (typically Service responses).
     * The same MSG_XXX are used to receive responses regarding the calls the
     * activity made before.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            // TODO

        }
    }
}
