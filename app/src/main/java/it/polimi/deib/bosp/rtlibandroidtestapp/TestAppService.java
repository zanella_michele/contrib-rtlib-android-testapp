package it.polimi.deib.bosp.rtlibandroidtestapp;

import android.content.Intent;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.annotation.Nullable;
import android.util.Log;

import bbque.rtlib.enumeration.RTLibResourceType;
import it.polimi.deib.bosp.androidservice.BarbequeService;
import bbque.rtlib.RTLib;
import bbque.rtlib.enumeration.RTLibExitCode;
import bbque.rtlib.exception.RTLibException;
import bbque.rtlib.exception.RTLibRegistrationException;
import bbque.rtlib.model.RTLibServices;

/**
 * Created by mzanella on 07/09/18.
 */

public class TestAppService extends BarbequeService {
    /**
     * So far the following value overlaps with the ones defined into
     * BbqueService
     * TODO: figure out how to use enum in this case, to be extended.
     */
    static final int MSG_CYCLES = 50;

    private static final String TAG = "TestAppService";
    private static final String APP_EXC_NAME = "BbqRTLibAndroidTestApp";
    private static final String APP_NAME = "BbqRTLibAndroidTestApp";

    private static int cycle_n = 1;

    @Override
    public void onCreate() {
        super.onCreate();
        RTLibExitCode response = RTLibExitCode.RTLIB_OK;
        try {
            // initialize the library
            rtlib = RTLib.init(APP_NAME);

            // register the application
            mEXC = new TestAppEXC(APP_EXC_NAME, APP_NAME, rtlib);

            Log.d(TAG, "Response from RTLIBInit is: "+  RTLib.getErrorStr(response));
        } catch (RTLibException e) {
            e.printStackTrace();
            String format = "[" + TAG + "] error occurred: %s";
            String message = RTLib.getErrorStr(e.getExitCode());
            Log.e(TAG, String.format(format, message));
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStarted");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroyed");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return cMessenger.getBinder();
    }

    /**
     * Instantiate the target - to be sent to clients - to communicate with this
     * instance of CustomService.
     */
    final Messenger cMessenger = new Messenger(new TestAppMessageHandler());

    /**
     * Handler of incoming messages from clients. This Handler overloads the
     * BbqueMessageHandler.
     */
    class TestAppMessageHandler extends BbqueMessageHandler {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_CYCLES:
                    Log.d(TAG, "Message cycles setting: " + msg.arg1);
                    cycle_n = msg.arg1;
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    /**
     * Follow the customized implementations of callback methods "onSomething".
     */

    protected class TestAppEXC extends AndroidBbqueEXC {

        public TestAppEXC(String name,
                          String recipe,
                          RTLibServices rtLibServices) throws RTLibRegistrationException {
            super(name, recipe, rtLibServices);
        }

        @Override
        public void onRun() throws RTLibException{
            int cycles = mEXC.cycles();
            Log.d(TAG, "onRun called, cycle: " + cycles+"/"+cycle_n);
            intent.putExtra("BBQ_DEBUG", "onRun called, cycle: " + cycles);
            sendBroadcast(intent);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {}
            if (cycles >= cycle_n) {
                Log.e(TAG, "Cycle number exceeded");
                throw new RTLibException(RTLibExitCode.RTLIB_EXC_WORKLOAD_NONE);
            }
        }

        @Override
        public void onConfigure(int awm_id) throws RTLibException {
            super.onConfigure(awm_id);
            int n_cores = 0;
            int n_cpus = 0;
            n_cores = getAssignedResources(RTLibResourceType.PROC_ELEMENT);
            n_cpus = getAssignedResources(RTLibResourceType.CPU);
            Log.d(TAG, "onConfigure "+awm_id+" assigned NumCPUs: "+n_cpus+" CoresQuota: "+n_cores);
        }


        @Override
        public void onMonitor() throws RTLibException {
            super.onMonitor();
            float goal = 2;
            if(mEXC.cycles()>4){
                Log.d(TAG, "onMonitor setCPSGoal to: " + goal);
                setCPSGoal(goal);
            }
        }

        @Override
        public void onRelease() {
            Log.d(TAG, "onRelease called");
            intent.putExtra("BBQ_DEBUG", "onRelease called");
            intent.putExtra("ON_RELEASE", 1);
            sendBroadcast(intent);
            //Uncomment the following line to test the terminate functionality
            //Currently it doesn't seem to work
//		terminate(cMessenger);
        }
    }
}
